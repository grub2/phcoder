/* pc.c - Read PC style partition tables.  */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2002,2004,2005,2006,2007,2008,2009  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/partition.h>
#include <grub/pc_partition.h>
#include <grub/disk.h>
#include <grub/mm.h>
#include <grub/misc.h>
#include <grub/dl.h>

static struct grub_partition_map grub_pc_partition_map;


static grub_err_t
pc_partition_map_iterate (grub_disk_t disk,
			  int (*hook) (grub_disk_t disk,
				       const grub_partition_t partition))
{
  struct grub_partition p;
  struct grub_pc_partition *pcdata;
  struct grub_pc_partition_mbr mbr;

  pcdata = grub_malloc (sizeof (*pcdata));
  p.offset = 0;
  pcdata->ext_offset = 0;
  p.data = pcdata;
  p.number = -1;
  p.partmap = &grub_pc_partition_map;

  /* Signature check is known to cause false positives
     because the same signature is used for bootsectors.  */
  if (disk->partition)
    {
      grub_free (pcdata);
      return grub_error (GRUB_ERR_BAD_PART_TABLE,
			 "pc partition can't be nested");
    }

  while (1)
    {
      int i;
      struct grub_pc_partition_entry *e;

      /* Read the MBR.  */
      if (grub_disk_read (disk, p.offset, 0, sizeof (mbr), &mbr))
	goto finish;

      /* Check if it is valid.  */
      if (mbr.signature != grub_cpu_to_le16 (GRUB_PC_PARTITION_SIGNATURE))
	{
	  grub_free (pcdata);
	  return grub_error (GRUB_ERR_BAD_PART_TABLE, "no signature");
	}

      /* Analyze DOS partitions.  */
      for (pcdata->index = 0; pcdata->index < 4; pcdata->index++)
	{
	  e = mbr.entries + pcdata->index;

	  p.start = p.offset + grub_le_to_cpu32 (e->start);
	  p.len = grub_le_to_cpu32 (e->length);
	  pcdata->dos_type = e->type;

	  grub_dprintf ("partition",
			"partition %d: flag 0x%x, type 0x%x, start 0x%llx, len 0x%llx\n",
			pcdata->index, e->flag, pcdata->dos_type,
			(unsigned long long) p.start,
			(unsigned long long) p.len);

	  /* If this is a GPT partition, this MBR is just a dummy.  */
	  if (e->type == GRUB_PC_PARTITION_TYPE_GPT_DISK && pcdata->index == 0)
	    {
	      grub_free (pcdata);
	      return grub_error (GRUB_ERR_BAD_PART_TABLE, "dummy mbr");
	    }

	  /* If this partition is a normal one, call the hook.  */
	  if (! grub_pc_partition_is_empty (e->type)
	      && ! grub_pc_partition_is_extended (e->type))
	    {
	      p.number++;

	      if (hook (disk, &p))
		return 1;
	    }
	  else if (p.number < 4)
	    /* If this partition is a logical one, shouldn't increase the
	       partition number.  */
	    p.number++;
	}

      /* Find an extended partition.  */
      for (i = 0; i < 4; i++)
	{
	  e = mbr.entries + i;

	  if (grub_pc_partition_is_extended (e->type))
	    {
	      p.offset = pcdata->ext_offset + grub_le_to_cpu32 (e->start);
	      if (! pcdata->ext_offset)
		pcdata->ext_offset = p.offset;

	      break;
	    }
	}

      /* If no extended partition, the end.  */
      if (i == 4)
	break;
    }

 finish:
  grub_free (pcdata);
  return grub_errno;
}


/* Partition map type.  */
static struct grub_partition_map grub_pc_partition_map =
  {
    .name = "pc_partition_map",
    .iterate = pc_partition_map_iterate,
  };

GRUB_MOD_INIT(pc_partition_map)
{
  grub_partition_map_register (&grub_pc_partition_map);
}

GRUB_MOD_FINI(pc_partition_map)
{
  grub_partition_map_unregister (&grub_pc_partition_map);
}
