/* usbreset.c - reset usb controllers */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2009  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/err.h>
#include <grub/command.h>
#include <grub/time.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/dl.h>
#include <grub/loader.h>
#include <grub/pci.h>

static grub_err_t
preboot_ehci (int noreturn __attribute__ ((unused)))
{
  auto int NESTED_FUNC_ATTR find_card (int bus, int dev, int func,
				       grub_pci_id_t pciid);

  int NESTED_FUNC_ATTR find_card (int bus, int dev, int func,
				  grub_pci_id_t pciid __attribute__ ((unused)))
  {
    grub_pci_address_t addr;
    grub_uint32_t class;
    grub_uint32_t subclass;
    grub_uint32_t base;
    grub_uint32_t eecp;

    addr = grub_pci_make_address (bus, dev, func, 2);

    class = grub_pci_read (addr);

    subclass = (class >> 16) & 0xFF;
    class >>= 24;

    /* If this is not an EHCI controller, just return.  */
    if (class != 0x0c || subclass != 0x03 || func != 7)
      return 0;

    /* Determine IO base address.  */
    addr = grub_pci_make_address (bus, dev, func, 4);
    base = grub_pci_read (addr);
    base &= ~0xff;

    eecp = *((grub_uint8_t *) (base + 9));

    if (! eecp)
      return 0;

    addr = grub_pci_make_address (bus, dev, func, 0);
    grub_pci_write_byte (addr + eecp + 3, 1);
    grub_pci_write_byte (addr + eecp + 2, 0);
    grub_pci_write_byte (addr + eecp + 4, 0);
    grub_pci_write_byte (addr + eecp + 5, 0);

    return 0;
  }

  grub_pci_iterate (find_card);

  return GRUB_ERR_NONE;
}

static grub_err_t
preboot_uhci (int noreturn __attribute__ ((unused)))
{
  auto int NESTED_FUNC_ATTR find_card (int bus, int dev, int func,
				       grub_pci_id_t pciid);

  int NESTED_FUNC_ATTR find_card (int bus, int dev, int func,
				  grub_pci_id_t pciid __attribute__ ((unused)))
  {
    grub_pci_address_t addr;
    grub_uint32_t class;
    grub_uint32_t subclass;
    grub_uint32_t base;

    addr = grub_pci_make_address (bus, dev, func, 2);

    class = grub_pci_read (addr);

    subclass = (class >> 16) & 0xFF;
    class >>= 24;

    /* If this is not an EHCI controller, just return.  */
    if (class != 0x0c || subclass != 0x03 || func == 7)
      return 0;

    /* Determine IO base address.  */
    addr = grub_pci_make_address (bus, dev, func, 8);
    base = grub_pci_read (addr);
    base = (base >> 5) & 0x7ff;

    addr = grub_pci_make_address (bus, dev, func, 0x30);

    grub_pci_write_word (addr, 0x8f00);

    grub_outw (base, 0x0002);
    grub_millisleep (10);
    grub_outw (base + 4, 0);
    grub_millisleep (10);
    grub_outw (base, 0);

    return 0;
  }

  grub_pci_iterate (find_card);

  return GRUB_ERR_NONE;
}


static grub_err_t
preboot_rest (void)
{
  return GRUB_ERR_NONE;
}

static grub_err_t
grub_cmd_ehcireset (grub_command_t cmd __attribute__ ((unused)),
		    int argc __attribute__ ((unused)),
		    char *args[] __attribute__ ((unused)))
{
  void *preb_handle;
  preb_handle
    = grub_loader_register_preboot_hook (preboot_ehci, preboot_rest, 
					 GRUB_LOADER_PREBOOT_HOOK_PRIO_CONSOLE);
  if (! preb_handle)
    return grub_errno;
  return GRUB_ERR_NONE;
}

static grub_err_t
grub_cmd_uhcireset (grub_command_t cmd __attribute__ ((unused)),
		    int argc __attribute__ ((unused)),
		    char *args[] __attribute__ ((unused)))
{
  void *preb_handle;
  preb_handle
    = grub_loader_register_preboot_hook (preboot_uhci, preboot_rest,
					 GRUB_LOADER_PREBOOT_HOOK_PRIO_CONSOLE);
  if (! preb_handle)
    return grub_errno;
  return GRUB_ERR_NONE;
}

static grub_command_t cmd_ehcireset, cmd_uhcireset;

GRUB_MOD_INIT(usbreset)
{
  (void) mod;			/* To stop warning. */
  cmd_ehcireset = grub_register_command ("ehcireset",
					 grub_cmd_ehcireset,
					 "ehcireset",
					 "Reset EHCI controller");
  cmd_uhcireset = grub_register_command ("uhcireset",
					 grub_cmd_uhcireset,
					 "uhcireset",
					 "Reset UHCI controller");
}

GRUB_MOD_FINI(usbreset)
{
  grub_unregister_command (cmd_ehcireset);
  grub_unregister_command (cmd_uhcireset);
}

