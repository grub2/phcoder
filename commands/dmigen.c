/* dmigen.c - generate smbios tables based on template. */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2009  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/err.h>
#include <grub/command.h>
#include <grub/efiemu/efiemu.h>
#include <grub/i386/pc/efiemu.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/file.h>
#include <grub/dl.h>
#include <grub/gzio.h>
#include <grub/acpi.h>

struct dmi_anchor
{
  grub_uint8_t magic[5];
  grub_uint8_t csum;
  grub_uint16_t data_size;
  grub_uint32_t data_addr;
  grub_uint16_t ntables;
  grub_uint8_t revision;
} __attribute__ ((packed));

struct sm_anchor
{
  grub_uint8_t magic[4];
  grub_uint8_t csum;
  grub_uint8_t anchor_length;
  grub_uint8_t ver_major;
  grub_uint8_t ver_minor;
  grub_uint16_t biggest_table;
  grub_uint8_t anchor_revision;
  grub_uint8_t pad[5];
  struct dmi_anchor dmi;
} __attribute__ ((packed));

struct dmi_table_header
{
  grub_uint8_t type;
  grub_uint8_t size;
  grub_uint16_t handle;
} __attribute__ ((packed));

static char *tables = 0;
static int tables_total_size = 0;
static int tablehandle = 0;
static int ntables = 0;
static int biggest_table = 0;

static int smvermajor = 2, smverminor = 1;

static void *
get_table (void *data __attribute__ ((unused)))
{
  struct sm_anchor *sm;
  sm = (struct sm_anchor *) grub_efiemu_mm_obtain_request (tablehandle);
  grub_memcpy (sm + 1, tables, tables_total_size);
  grub_memcpy (sm->magic, "_SM_", 4);
  sm->anchor_length = sizeof (*sm);
  sm->ver_major = smvermajor;
  sm->ver_minor = smverminor;
  sm->anchor_revision = 0;
  grub_memset (sm->pad, 0, sizeof (sm->pad));
  grub_memcpy (sm->dmi.magic, "_DMI_", 5);
  sm->dmi.data_addr = PTR_TO_UINT32 (sm + 1);
  grub_dprintf ("dmigen", "new SMBIOS at %p\n", sm + 1);
  sm->dmi.data_size = tables_total_size;
  sm->dmi.revision = ((smvermajor & 0xf) << 4) | smverminor;
  sm->biggest_table = biggest_table;
  sm->dmi.ntables = ntables;
  sm->dmi.csum = 0;
  sm->dmi.csum = 1 + ~grub_byte_checksum ((grub_uint8_t *) &(sm->dmi),
					  sizeof (sm->dmi));
  sm->csum = 0;
  sm->csum = 1 + ~grub_byte_checksum ((grub_uint8_t *) sm, sizeof (*sm));
  return sm;
}

static void
unload (void *data __attribute__ ((unused)))
{
  grub_efiemu_mm_return_request (tablehandle);
  grub_free (tables);
  tablehandle = 0;
  tables = 0;
  tables_total_size = 0;
}

static grub_err_t
grub_cmd_efiemu_dmigen (grub_command_t cmd __attribute__ ((unused)),
			int argc, char *args[])
{
  grub_efi_guid_t smbios = GRUB_EFI_SMBIOS_TABLE_GUID;
  char *ptr;
  char *inptr, *outptr;
  grub_file_t file;
  struct sm_anchor *sm;
  int excluded[256], handle;
  grub_size_t fsize;
  grub_err_t err;
  char handles[65536];

  if (argc != 1)
    return grub_error (GRUB_ERR_BAD_ARGUMENT, "template required");

  err = grub_efiemu_autocore ();
  if (err)
    return err;

  file = grub_gzfile_open (args[0], 1);
  if (! file)
    return grub_error (GRUB_ERR_FILE_NOT_FOUND, "couldn't read %s");

  fsize = grub_file_size (file);

  for (ptr = (char *) 0xf0000; ptr < (char *) 0x100000; ptr += 16)
    if (grub_memcmp (ptr, "_SM_", 4) == 0
	&& grub_byte_checksum ((grub_uint8_t *) ptr,
			       ((struct sm_anchor *)ptr)->anchor_length) == 0)
      break;

  if (ptr < (char *) 0x100000)
    {
      sm = (struct sm_anchor *) ptr;
      tables_total_size = sm->dmi.data_size;
      smvermajor = sm->ver_major;
      smverminor = sm->ver_minor;
    }
  else
    {
      smvermajor = 2;
      smverminor = 1;
      sm = 0;
      tables_total_size = 0;
    }

  tables_total_size += fsize;
  tables = grub_malloc (tables_total_size);
  ntables = 0;
  biggest_table = 0;
  if (! tables)
    {
      grub_file_close (file);
      return grub_error (GRUB_ERR_OUT_OF_MEMORY,
			 "couldn't allocate space for new tables");
    }

  grub_ssize_t read;
  read = grub_file_read (file, tables, fsize);

  if (read != (grub_ssize_t) fsize)
    {
      grub_dprintf ("dmigen", "read %d instead of %d\n", (int)read, (int)fsize);

      grub_file_close (file);
      grub_free (tables);
      tables_total_size = 0;
      tables = 0;
      return grub_errno;
    }
  grub_file_close (file);

  grub_memset (excluded, 0, sizeof (excluded));
  grub_memset (handles, 0, sizeof (handles));

  for (inptr = tables; inptr < tables + fsize; )
    {
      excluded[((struct dmi_table_header *) inptr)->type] = 1;
      ptr = inptr;
      ptr += ((struct dmi_table_header *) inptr)->size;
      if (((struct dmi_table_header *) inptr)->size == 0)
	{
	  fsize = inptr - tables;
	  break;
	}
      while (ptr + 2 < tables + fsize && (ptr[0] != 0 || ptr[1] != 0))
	ptr++;
      ptr += 2;
      ntables++;
      if (biggest_table < ptr - inptr)
	biggest_table = ptr - inptr;
      inptr = ptr;
    }
  grub_dprintf ("dmigen", "SM at 0x%x, length %d\n",
		(int) sm->dmi.data_addr, (int) sm->dmi.data_size);
  for (inptr = (char *) UINT_TO_PTR (sm->dmi.data_addr),
	 outptr = tables + fsize;
       inptr < (char *) UINT_TO_PTR (sm->dmi.data_addr)
	 + sm->dmi.data_size; )
    {
      ptr = inptr;
      ptr += ((struct dmi_table_header *) inptr)->size;
      if (((struct dmi_table_header *) inptr)->size == 0)
	break;
      while ((ptr + 2 < (char *) UINT_TO_PTR (sm->dmi.data_addr)
	      + sm->dmi.data_size) && (ptr[0] != 0 || ptr[1] != 0))
	ptr++;
      ptr += 2;

      if (! excluded[((struct dmi_table_header *) inptr)->type])
	{
	  grub_memcpy (outptr, inptr, ptr - inptr);
	  outptr += ptr - inptr;
	  ntables++;
	  if (biggest_table < ptr - inptr)
	    biggest_table = ptr - inptr;
	  handles[((struct dmi_table_header *) inptr)->handle] = 1;
	}
      inptr = ptr;
    }
  tables_total_size = outptr - tables;

  handle = 0;
  for (inptr = tables; inptr < tables + fsize; )
    {
      if (handles[((struct dmi_table_header *) inptr)->handle])
	{
	  while (handle < 0xfefe && handles[handle])
	    handle++;
	  ((struct dmi_table_header *) inptr)->handle = handle;
	}
      ptr = inptr;
      ptr += ((struct dmi_table_header *) inptr)->size;
      while (ptr + 2 < tables + fsize && (ptr[0] != 0 || ptr[1] != 0))
	ptr++;
      ptr += 2;
      inptr = ptr;
    }
  grub_dprintf ("dmigen", "tables_total_size=%d\n", tables_total_size);

  tables = grub_realloc (tables, tables_total_size);
  if (! tables)
    return grub_error (GRUB_ERR_OUT_OF_MEMORY,
		       "couldn't allocate space for new tables");

  tablehandle = grub_efiemu_request_memalign (16, tables_total_size
					      + sizeof (struct dmi_anchor),
					      GRUB_EFI_RUNTIME_SERVICES_DATA);
  if (tablehandle < 0)
    {
      tablehandle = 0;
      grub_free (tables);
      tables = 0;
      tables_total_size = 0;
      return grub_error (GRUB_ERR_OUT_OF_MEMORY,
			 "couldn't allocate space for new tables");
    }

  err = grub_efiemu_register_configuration_table (smbios, get_table,
						  unload, 0);

  if (err)
    {
      grub_efiemu_mm_return_request (tablehandle);
      grub_free (tables);
      tablehandle = 0;
      tables = 0;
      tables_total_size = 0;
      return err;
    }

  return GRUB_ERR_NONE;
}

static grub_command_t cmd_dmigen;

GRUB_MOD_INIT(dmigen)
{
  cmd_dmigen = grub_register_command ("efiemu_dmigen",
				      grub_cmd_efiemu_dmigen,
				      "efiemu_dmigen TEMPLATE",
				      "Generate DMI tables");
}

GRUB_MOD_FINI(dmigen)
{
  grub_unregister_command (cmd_dmigen);
}

