/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2009  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/mm.h>
#include <grub/misc.h>
#include <grub/env.h>
#include <grub/menu_viewer.h>
#include <grub/menu.h>

/* The list of menu viewers.  */
static grub_menu_viewer_t menu_viewer_list;

static int should_return;
static int menu_viewer_changed;

void
grub_menu_viewer_register (grub_menu_viewer_t viewer)
{
  viewer->next = menu_viewer_list;
  menu_viewer_list = viewer;
}

static grub_menu_viewer_t get_current_menu_viewer (void)
{
  const char *selected_name = grub_env_get ("menuviewer");
  grub_menu_viewer_t cur;

  /* If none selected, pick the last registered one.  */
  if (selected_name == 0)
    return menu_viewer_list;

  for (cur = menu_viewer_list; cur; cur = cur->next)
    {
      if (grub_strcmp (cur->name, selected_name) == 0)
        return cur;
    }

  /* Fall back to the first entry (or null).  */
  return menu_viewer_list;
}

grub_err_t
grub_menu_viewer_show_menu (grub_menu_t menu, int nested)
{
  int repeat = 0;
  do
    {
      grub_menu_viewer_t cur;
      repeat = 0;
      menu_viewer_changed = 0;
      cur = get_current_menu_viewer ();
      if (! cur)
        return grub_error (GRUB_ERR_BAD_ARGUMENT, "no menu viewer available.");

      should_return = 0;
      cur->show_menu (menu, nested);
      if (grub_errno != GRUB_ERR_NONE)
        {
          grub_print_error ();
          grub_wait_after_message ();
        }
      if (menu_viewer_changed)
        repeat = 1;
    }
  while (repeat);
  return grub_errno;
}

int
grub_menu_viewer_should_return (void)
{
  return should_return;
}

static char *
menuviewer_write_hook (struct grub_env_var *var __attribute__ ((unused)),
                       const char *val)
{
  menu_viewer_changed = 1;
  should_return = 1;
  return grub_strdup (val);
}

void
grub_menu_viewer_init (void)
{
  grub_register_variable_hook ("menuviewer", 0, menuviewer_write_hook);
}

